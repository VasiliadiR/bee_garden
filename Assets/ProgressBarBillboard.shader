﻿Shader "ProgressBarBillboard"
{
    Properties
    {
        _Value ("Value", Range (0, 1)) = 0.5
        _ColorFilled ("Filled", Color) = (0.2, 1, 0.2, 1)
        _ColorEmpty ("Empty", Color) = (0.01, 0.6, 0.1, 1)
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        ZWrite Off
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM

            #pragma vertex vert  
            #pragma fragment frag

            uniform half _Value;
            uniform half4 _ColorEmpty;
            uniform half4 _ColorFilled;

            struct appdata
            {
                float4 vertex : POSITION;
                half2 uv : TEXCOORD0;
            };
            struct v2f
            {
                float4 pos : SV_POSITION;
                half2 uv : TEXCOORD0;
            };

            v2f vert(appdata v)
            {
                v2f o;
                
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                /*
                float3 vpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz);
                float4 worldCoord = float4(unity_ObjectToWorld._m03, unity_ObjectToWorld._m13, unity_ObjectToWorld._m23, 1);
                float4 viewPos = mul(UNITY_MATRIX_V, worldCoord) + float4(vpos, 0);
                float4 outPos = mul(UNITY_MATRIX_P, viewPos);

                o.pos = outPos;
*/

                return o;
            }

            float4 frag(v2f i) : COLOR
            {
                if (i.uv.r > _Value) 
                {
                    return _ColorEmpty;
                }
                return _ColorFilled;
            }

            ENDCG
        }
    }
}
