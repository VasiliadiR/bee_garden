﻿namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс перехода состояния
    /// </summary>
    public class StateTransition
    {
        private readonly IGameState currentState;
        private readonly GameFSMCommand command;

        public StateTransition(IGameState _currentState, GameFSMCommand _command)
        {
            currentState = _currentState;
            command = _command;
        }

        public override int GetHashCode()
        {
            return 17 + 31 * currentState.GetHashCode() + 31 * command.GetHashCode();
        }

        public override bool Equals(object _obj)
        {
            var other = _obj as StateTransition;
            return other != null && this.currentState == other.currentState && this.command == other.command;
        }
    }
}