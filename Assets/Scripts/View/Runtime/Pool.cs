﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс пула объектов
/// </summary>
public class Pool : MonoBehaviour
{
    /// <summary>
    /// Объект, который будет клонирован
    /// </summary>
    [SerializeField] private GameObject particle;
    
    /// <summary>
    /// Размер пула
    /// </summary>
    [SerializeField] private int size = 0;
    
    /// <summary>
    /// Список объектов
    /// </summary>
    [SerializeField]
    private List<GameObject> list = new List<GameObject>();
    
    private void Awake()
    {
        CreatePool();  
    }
    public void CreatePool()
    {
        for (var k = 0; k < size; k++)
        {
            var unit = Instantiate(particle, gameObject.transform);
            unit.SetActive(false);  // пул изначально состоит из неактивных объектов
            unit.name = unit.name.Replace("(Clone)","") + list.Count;
            list.Add(unit);
        }
    }

    /// <summary>
    /// Извлечение из пула идет по принципу первого свободного неактивного объекта
    /// </summary>
    public GameObject Pop() 
    {
        for (var i = 0; i < list.Count; i++)
        {
            if (!list[i].activeSelf)
                return list[i];
            if (i > list.Count * 0.9f)
                CreatePool();    // Увеличение пула, если нужно еще место
        }
        return null;
    }
}