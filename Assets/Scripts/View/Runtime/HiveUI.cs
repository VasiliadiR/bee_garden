﻿using System;
using BeeGarden.Unity;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс управления интерфейсом улья
/// </summary>
public class HiveUI : MonoBehaviour
{
    [SerializeField] private Button closeButton;
    [SerializeField] private Button collectButton;
    
    [SerializeField] private Text honey;
    [SerializeField] private Text bees;
    [SerializeField] private Text workers;
    [SerializeField] private Text drones;

    private float currentHoney;
    private float honeyMax;
    private int currentBees;
    private int beesMax;
    private int currentWorkers;
    private int currentDrones;

    private BeehiveView currentBeehive;

    #region Properties
    
    public float CurrentHoney
    {
        get => currentHoney;
        set => currentHoney = value;
    }

    public int CurrentBees
    {
        get => currentBees;
        set => currentBees = value;
    }

    public int CurrentWorkers
    {
        get => currentWorkers;
        set => currentWorkers = value;
    }

    public int CurrentDrones
    {
        get => currentDrones;
        set => currentDrones = value;
    }

    public BeehiveView CurrentBeehive
    {
        get => currentBeehive;
        set => currentBeehive = value;
    }

    public float HoneyMax
    {
        get => honeyMax;
        set => honeyMax = value;
    }

    public int BeesMax
    {
        get => beesMax;
        set => beesMax = value;
    }

    #endregion
    
    private void OnEnable()    
    { 
        collectButton.onClick.AddListener(TakeHoney);
        closeButton.onClick.AddListener(Close);
    }
    
    /// <summary>
    /// Забрать мед из улья
    /// </summary>
    private void TakeHoney()
    {
        float honeyCount = 0;
        honeyCount = PlayerPrefs.HasKey("HoneyCount") ? PlayerPrefs.GetFloat("HoneyCount") : 0;
        honeyCount += currentBeehive.GiveHoney();
        PlayerPrefs.SetFloat("HoneyCount", honeyCount);
        
        collectButton.interactable = false;
    }
    private void Close()
    {
        currentBeehive.Transmit = false;
        gameObject.SetActive(false);
    }
    private void Update()
    {
        if (currentBeehive.CheckFullHoney())
            collectButton.interactable = true;

        honey.text = String.Format( "{0}/{1}", currentHoney, honeyMax);
        bees.text = String.Format( "{0}/{1}", currentBees, beesMax);
        workers.text = CurrentWorkers.ToString();
        drones.text = CurrentDrones.ToString();
    }

    private void OnDisable()
    {
        collectButton.onClick.RemoveAllListeners();
        closeButton.onClick.RemoveAllListeners();
    }
}
