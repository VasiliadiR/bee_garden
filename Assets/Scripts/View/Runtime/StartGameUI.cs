﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс UI окна начала игры
    /// </summary>
    public class StartGameUI : MonoBehaviour
    {
        [SerializeField] private BeehiveManagerView beehiveManagerView;
        [SerializeField] private FlowerManagerView flowerManagerView;
        
        [Header("Настройки для размещения ульев")] 
        [SerializeField] private InputField hivesCount;
        [Header("Настройки для менеджера цветов")]
        [SerializeField] private InputField flowersStartCount;
        [SerializeField] private InputField flowersMaxCount;
        [SerializeField] private InputField flowersBornTime;
        [Header("Настройки для улья")]
        [SerializeField] private InputField hiveMaxHoney;
        [SerializeField] private InputField hiveMaxBee;
        [SerializeField] private InputField hiveStartBee;
        [SerializeField] private InputField hiveBornTime;
        [SerializeField] private InputField hiveDieTime;
        [SerializeField] private InputField hiveDroneCoefficient;
        [SerializeField] private InputField hiveGenocideCoefficient;
        [Header("Настройки для пчелы")]
        [SerializeField] private InputField beeSpeed;
        [SerializeField] private InputField workerCapacity;
        [SerializeField] private InputField workerCollectionSpeed;
        [SerializeField] private InputField workerFlowerDistance;
        [Header("Настройки для цветов")]
        [SerializeField] private InputField flowerMaxHoney;
        [SerializeField] private InputField flowerStartHoney;
        [SerializeField] private InputField flowerHoneyRegeneration;
        [SerializeField] private InputField flowerMaxBee;
        
        
        private void OnEnable()
        {
            hivesCount.onEndEdit.AddListener(delegate { CheckCoefficient(hivesCount, 0, 10); });
            flowersStartCount.onEndEdit.AddListener(delegate { CheckCoefficient(flowersStartCount, 0, 1000); });
            flowersMaxCount.onEndEdit.AddListener(delegate { CheckCoefficient(flowersMaxCount, 0, 1000); });
            flowersBornTime.onEndEdit.AddListener(delegate { CheckCoefficient(flowersBornTime, 0, 100); });
            hiveMaxHoney.onEndEdit.AddListener(delegate { CheckCoefficient(hiveMaxHoney, 0, 9999); });
            hiveMaxBee.onEndEdit.AddListener(delegate { CheckCoefficient(hiveMaxBee, 0, 1000); });
            hiveStartBee.onEndEdit.AddListener(delegate { CheckCoefficient(hiveStartBee, 0, 1000); });
            hiveBornTime.onEndEdit.AddListener(delegate { CheckCoefficient(hiveBornTime, 0, 100); });
            hiveDieTime.onEndEdit.AddListener(delegate { CheckCoefficient(hiveDieTime, 0, 1000); });
            hiveDroneCoefficient.onEndEdit.AddListener(delegate { CheckCoefficient(hiveDroneCoefficient, 0, 1); });
            hiveGenocideCoefficient.onEndEdit.AddListener(delegate { CheckCoefficient(hiveGenocideCoefficient, 0, 1); });
            beeSpeed.onEndEdit.AddListener(delegate { CheckCoefficient(beeSpeed, 0, 100); });
            workerCapacity.onEndEdit.AddListener(delegate { CheckCoefficient(workerCapacity, 0, 100); });
            workerCollectionSpeed.onEndEdit.AddListener(delegate { CheckCoefficient(workerCollectionSpeed, 0, 10); });
            workerFlowerDistance.onEndEdit.AddListener(delegate { CheckCoefficient(workerFlowerDistance, 0, 2); });
            flowerMaxHoney.onEndEdit.AddListener(delegate { CheckCoefficient(flowerMaxHoney, 0, 1000); });
            flowerStartHoney.onEndEdit.AddListener(delegate { CheckCoefficient(flowerStartHoney, 0, 1000); });
            flowerHoneyRegeneration.onEndEdit.AddListener(delegate { CheckCoefficient(flowerHoneyRegeneration, 0, 5); });
            flowerMaxBee.onEndEdit.AddListener(delegate { CheckCoefficient(flowerMaxBee, 0, 10); });
        }

        /// <summary>
        /// Проверка на пригодность вводимого символа
        /// </summary>
        public void CheckCoefficient (InputField _inputField, float _numberMin, float _numberMax)
        {
            float fieldNumber = 0;
            if(_inputField.text.Length != 0)
                fieldNumber = float.Parse(_inputField.text, CultureInfo.InvariantCulture.NumberFormat);
            if (fieldNumber <= _numberMin || fieldNumber > _numberMax)
                _inputField.text = Mathf.Clamp(fieldNumber, _numberMin, _numberMax).ToString(CultureInfo.InvariantCulture); 
        }
        
        /// <summary>
        ///Начинает инициализацию при нажатии кнопки запуска игры
        /// </summary>
        public void StartGame()
        {
            var _hiveCount = int.Parse(hivesCount.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hiveMaxHoney = float.Parse(hiveMaxHoney.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hiveMaxBeeCount = int.Parse(hiveMaxBee.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hiveStartBeeCount = int.Parse(hiveStartBee.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hiveNewBornTime = float.Parse(hiveBornTime.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hiveDieTime = float.Parse(this.hiveDieTime.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hiveDroneCoefficient =
                float.Parse(this.hiveDroneCoefficient.text, CultureInfo.InvariantCulture.NumberFormat);
            var _hivedroneGenocideCoefficient =
                float.Parse(hiveGenocideCoefficient.text, CultureInfo.InvariantCulture.NumberFormat);
            var _beeSpeed = float.Parse(this.beeSpeed.text, CultureInfo.InvariantCulture.NumberFormat);
            var _workerMaxCapacityHoney = float.Parse(workerCapacity.text, CultureInfo.InvariantCulture.NumberFormat);
            var _workerCollectionSpeed =
                float.Parse(this.workerCollectionSpeed.text, CultureInfo.InvariantCulture.NumberFormat);
            var _workerFlowerDistance =
                float.Parse(this.workerFlowerDistance.text, CultureInfo.InvariantCulture.NumberFormat);
            
            var _fieldStartCount = int.Parse(flowersStartCount.text, CultureInfo.InvariantCulture.NumberFormat);
            var _fieldMaxCount = int.Parse(flowersMaxCount.text, CultureInfo.InvariantCulture.NumberFormat);
            var _fieldBornTime = int.Parse(flowersBornTime.text, CultureInfo.InvariantCulture.NumberFormat);

            var _flowerMaxHoney = float.Parse(flowerMaxHoney.text, CultureInfo.InvariantCulture.NumberFormat);
            var _flowerStartHoney = float.Parse(flowerStartHoney.text, CultureInfo.InvariantCulture.NumberFormat);
            var _flowerHoneyRegeneration = float.Parse(flowerHoneyRegeneration.text, CultureInfo.InvariantCulture.NumberFormat);
            var _flowerMaxBeeCount = int.Parse(flowerMaxBee.text, CultureInfo.InvariantCulture.NumberFormat);
            
            beehiveManagerView.Initialize(_hiveCount, _hiveMaxHoney, _hiveMaxBeeCount, _hiveStartBeeCount,
                _hiveNewBornTime,
                _hiveDieTime, _hiveDroneCoefficient, _hivedroneGenocideCoefficient, _beeSpeed, _workerMaxCapacityHoney,
                _workerCollectionSpeed, _workerFlowerDistance);
            
            flowerManagerView.Initialize(_fieldStartCount, _fieldMaxCount, _fieldBornTime, _flowerMaxHoney,
                _flowerStartHoney, _flowerHoneyRegeneration, _flowerMaxBeeCount);

            gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            hivesCount.onEndEdit.RemoveAllListeners();
            flowersStartCount.onEndEdit.RemoveAllListeners();
            flowersMaxCount.onEndEdit.RemoveAllListeners();
            flowersBornTime.onEndEdit.RemoveAllListeners();
            hiveMaxHoney.onEndEdit.RemoveAllListeners();
            hiveMaxBee.onEndEdit.RemoveAllListeners();
            hiveStartBee.onEndEdit.RemoveAllListeners();
            hiveBornTime.onEndEdit.RemoveAllListeners();
            hiveDieTime.onEndEdit.RemoveAllListeners();
            hiveDroneCoefficient.onEndEdit.RemoveAllListeners();
            hiveGenocideCoefficient.onEndEdit.RemoveAllListeners();
            beeSpeed.onEndEdit.RemoveAllListeners();
            workerCapacity.onEndEdit.RemoveAllListeners();
            workerCollectionSpeed.onEndEdit.RemoveAllListeners();
            workerFlowerDistance.onEndEdit.RemoveAllListeners();
            flowerMaxHoney.onEndEdit.RemoveAllListeners();
            flowerStartHoney.onEndEdit.RemoveAllListeners();
            flowerHoneyRegeneration.onEndEdit.RemoveAllListeners();
            flowerMaxBee.onEndEdit.RemoveAllListeners();
        }
    }
}