﻿using BeeGarden.Model;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс представления в юнити класса цветка
    /// </summary>
    public class FlowerView : MonoBehaviour
    {
        [SerializeField] private Flower flower;   

        public void Initialize(Flower _flower)
        {
            flower = _flower;
            transform.position = _flower.Position;
            flower.OnDie += MementoMori;
        }
        

        /// <summary>
        /// Уничтожает цветок 
        /// </summary>
        /// <param name="_flower"></param>
        private void MementoMori(Flower _flower)
        {
            flower.OnDie -= MementoMori;
            gameObject.SetActive(false);
        }
    }
}