﻿using BeeGarden.Model;
using UnityEngine;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс представления в юнити класса рабочей пчелы
    /// </summary>
    public class WorkerView : MonoBehaviour
    {
                
        private static readonly int Value = Shader.PropertyToID("_Value");

        private MaterialPropertyBlock mpb;

        [SerializeField] private Renderer workerRenderer;
        
        [SerializeField] private Worker worker;

        private Vector3 currentTarget;

        private float targetAngle;
        public void Initialize(Worker _worker)
        {
            worker = _worker;
            transform.position = worker.Position;
            worker.HideView += HideView;
            worker.HoneyImport += ChangeMat;
            
            mpb = new MaterialPropertyBlock();
            ChangeMat(0);

        }

        private void Update()
        {
            transform.position = worker.Position;
            transform.rotation = Quaternion.LookRotation(worker.Direction);
        }
        
        /// <summary>
        /// Отправиться в улей
        /// </summary>
        private void HideView(Worker _worker)
        {
            if (_worker != worker) return;   
            worker.HideView -= HideView;
            worker.HoneyImport -= ChangeMat;
            gameObject.SetActive(false);
        }
        private void ChangeMat(float _value)
        {
            workerRenderer.GetPropertyBlock(mpb);
            mpb.SetFloat(Value, _value);
            workerRenderer.SetPropertyBlock(mpb);
        }
        
    }
}