﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace BeeGarden.Unity
{
    /// <summary>
    /// Класс UI окна статистики
    /// </summary>
    public class StatisticUI : MonoBehaviour
    {
        [SerializeField] private Text hivesCount;
        [SerializeField] private Text flowersCount;
        [SerializeField] private Text worksCount;
        [SerializeField] private Text dronesCount;
        [SerializeField] private Text honeyCount;
        [SerializeField] private Text sessionCount;
        [SerializeField] private Text timeCount;

        private void Start()
        {
            StartStatistic();
        }
        
        /// <summary>
        /// Данные для статистики берутся из сохранений
        /// </summary>
        private void StartStatistic()
        {
            hivesCount.text = PlayerPrefs.HasKey("HivesCount") ? PlayerPrefs.GetInt("HivesCount").ToString() : string.Empty;
            flowersCount.text = PlayerPrefs.HasKey("FlowersCount") ? PlayerPrefs.GetInt("FlowersCount").ToString() : string.Empty;
            worksCount.text = PlayerPrefs.HasKey("WorkersCount") ? PlayerPrefs.GetInt("WorkersCount").ToString() : string.Empty;
            dronesCount.text = PlayerPrefs.HasKey("DronesCount") ? PlayerPrefs.GetInt("DronesCount").ToString() : string.Empty;
            honeyCount.text = PlayerPrefs.HasKey("HoneyCount") ? PlayerPrefs.GetFloat("HoneyCount").ToString() : string.Empty;
            sessionCount.text = PlayerPrefs.HasKey("SessionCount") ? PlayerPrefs.GetInt("SessionCount").ToString() : string.Empty;
            
            var counter = PlayerPrefs.HasKey("TimeCounter") ? PlayerPrefs.GetFloat("TimeCounter") : 0;

            TimeSpan interval = TimeSpan.FromSeconds(counter);
            var timeText = interval.ToString();
            timeText = timeText.Substring(0, timeText.Length - 7);

            timeCount.text = timeText;
        }
    }
}