﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BeeGarden.Model 
{
    /// <summary>
    /// Класс цветка
    /// </summary>
    [Serializable]
    public class Flower : IUpdatable
    {
        #region Params
        
        /// <summary>
        /// Максимальная емкость цветка
        /// </summary>
        [SerializeField] private float maxCapacity;

        /// <summary>
        /// Текущее количество меда
        /// </summary>
        [SerializeField] private float currentVolume;

        /// <summary>
        /// Скорость наполнения цветка медом
        /// </summary>
        [SerializeField] private float honeyRegeneration;

        /// <summary>
        /// Максимальное число пчел, обслуживающих цветок
        /// </summary>
        [SerializeField] private int maxBeeCount;

        /// <summary>
        /// Уникальный номер цветка
        /// </summary>
        [SerializeField]  private int arrayIndex;

        /// <summary>
        /// Текущее число пчел, находящихся на цветке
        /// </summary>
        private int currentBeeCount;  

        [NonSerialized] private List<Worker> workersInFlower = new List<Worker>();
        
        /// <summary>
        /// Счетчик времени
        /// </summary>
        private float timeCounter;

        /// <summary>
        /// Место цветка в этом мире
        /// </summary>
        private Vector3 position;
        
        #endregion

        public Vector3 Position => position;

        public int ArrayIndex
        {
            get => arrayIndex;
            set => arrayIndex = value;
        }

        /// <summary>
        /// Событие смерти цветка
        /// </summary>
        public event Action<Flower> OnDie;

        #region Methods
        
        public Flower(int _arrayIndex, float _maxCapacity, float _currentVolume, float _honeyRegeneration, int _maxBeeCount, Vector3 _position)
        {
            arrayIndex = _arrayIndex;
            maxCapacity = _maxCapacity;
            currentVolume = _currentVolume;
            honeyRegeneration = _honeyRegeneration;
            maxBeeCount = _maxBeeCount;
            position = _position;
        }
        
        public void Update(float _deltaTime) 
        {
            if (timeCounter < 1)
                timeCounter += _deltaTime;
            else
            {
                if (currentVolume < maxCapacity)
                    currentVolume += honeyRegeneration;
                timeCounter -= 1;
            }
            if (currentVolume <= 0)
                Die();
        }
        
        /// <summary>
        /// Передача меда пчеле
        /// </summary>
        public float GiveHoney(float _honey)
        {
            float exportHoney;
            if (currentVolume > _honey)
                exportHoney = _honey;
            else
                exportHoney = currentVolume;
            currentVolume -= exportHoney;
            return exportHoney;
        }
        
        /// <summary>
        /// Метод, определяющий, может ли пчела сесть на цветок
        /// </summary>
        /// <param name="_worker">Пчела рабочая</param>
        /// <param name="_record">Флаг на запись, true: пчела хочет сесть на цветок, false: пчела улетает с цветка</param>
        /// <returns></returns>
        public bool CounterBee(Worker _worker, bool _record)
        {
            if (!workersInFlower.Contains(_worker) && _record)
            {
                if (workersInFlower.Count < maxBeeCount)
                {
                    workersInFlower.Add(_worker);
                    return true;
                }
                return false;
            }
            if (workersInFlower.Contains(_worker) && !_record)
            {
                var last = workersInFlower.Count - 1;
                var current = workersInFlower.IndexOf(_worker);
                if (current != last)
                {
                    workersInFlower[current] = workersInFlower[last];
                }
                workersInFlower.RemoveAt(last);
                return true;
            }
            return true;
        }
        private void Die()  
        {
            OnDie?.Invoke(this);
        }
        
        #endregion
    }
}