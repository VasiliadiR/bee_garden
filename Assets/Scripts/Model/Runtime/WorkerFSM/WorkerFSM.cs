﻿using System;
using System.Collections.Generic;

namespace Model.Runtime.WorkerFSM
{
    public class WorkerFSM
    {
        private Dictionary<WorkerStateTransition, IWorkerState> transitions;
        private IWorkerState currentState;

        public IWorkerState CurrentState
        {
            get => currentState;
            set => currentState = value;
        }

        public void Initialize( Dictionary<WorkerStateTransition, IWorkerState> _transitions, IWorkerState _startState)
        {
            CurrentState = _startState;
            transitions = _transitions;
            CurrentState.Enter();
        }
        /// <summary>
        /// Запрос на следующее состояние
        /// </summary>
        private IWorkerState GetNext(WorkerFSMComand command)
        {
            var transition = new WorkerStateTransition(CurrentState, command);
            IWorkerState nextState;
            if (!transitions.TryGetValue(transition, out nextState))
                throw new Exception("Invalid transition: " + CurrentState + " -> " + command);
            return nextState;
        }

        /// <summary>
        /// Переход к следующему состоянию
        /// </summary>
        public void MoveNext(WorkerFSMComand command)
        {
            CurrentState.Exit();
            CurrentState = GetNext(command);
            CurrentState.Enter();
        }

        public void Update(float deltaTime)
        {
            CurrentState.Update(deltaTime);
        }
    }
}