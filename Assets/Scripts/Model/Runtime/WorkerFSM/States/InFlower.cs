﻿using BeeGarden.Model;

namespace Model.Runtime.WorkerFSM.States
{
    public class InFlower : IWorkerState
    {
        private float timeCounter;
        private Worker worker;

        public Worker Worker
        {
            get => worker;
            set => worker = value;
        }
        public void Update(float _deltaTime)
        {
            if (worker.CurrentFlower != null)
            {
                if (timeCounter < 1)
                    timeCounter += _deltaTime;
                else
                {
                    timeCounter -= 1;
                    var addHoney = worker.CurrentFlower.GiveHoney(worker.CollectionSpeed);
                    if (worker.MAXCapacityHoney - worker.CurrentHoney < addHoney)
                        worker.CurrentHoney += worker.MAXCapacityHoney - worker.CurrentHoney;
                    else
                        worker.CurrentHoney += addHoney;
                    worker.HoneyProgressBar();
                    
                    FullTank();
                }
            }
            else
            {
                worker.WorkerFsm.MoveNext(WorkerFSMComand.FlyToFlower);
            }
        }

        private void FullTank()
        {
            if (worker.CurrentHoney >= worker.MAXCapacityHoney) //пчела полностью заполнила свою емкость 
            {
                worker.CurrentFlower.CounterBee(worker, false);
                worker.WorkerFsm.MoveNext(WorkerFSMComand.FlyToHive);
            }
        }

        public void Enter()
        {
            FlowerOverflow();
        }

        public void Exit()
        {
            if (worker.CurrentFlower != null)
            {
                worker.CurrentFlower.OnDie -= OnFlowerDie;
                worker.CurrentFlower = null;
            }
        }

        private void FlowerOverflow()
        {
            if (!worker.CurrentFlower.CounterBee(worker, true)) //цветок переполнен
            {
                worker.CurrentFlower = null;
                worker.WorkerFsm.MoveNext(WorkerFSMComand.FlyToFlower);
            }
            else
                worker.CurrentFlower.OnDie += OnFlowerDie;
        }

        private void OnFlowerDie(Flower _flower)
        {
            worker.CurrentFlower.OnDie -= OnFlowerDie;
            worker.CurrentFlower = null;
            worker.WorkerFsm.MoveNext(WorkerFSMComand.FlyToFlower);
        }
    }
}