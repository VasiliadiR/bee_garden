﻿namespace BeeGarden.Model
{
    public interface IUpdatable
    {
        void Update(float _deltaTime);
    }
}