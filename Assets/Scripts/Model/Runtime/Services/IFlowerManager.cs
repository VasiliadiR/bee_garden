﻿using System.Collections.Generic;
using Model.Runtime.Services;

namespace BeeGarden.Model
{
    public interface IFlowerManager : IService
    {
        List<Flower> GetFlowerList();
    }
}