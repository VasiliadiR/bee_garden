﻿using System;
using System.Collections.Generic;
using Model.Runtime.Services;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BeeGarden.Model
{
    /// <summary>
    /// Класс менеджера управления всеми цветами
    /// </summary>
    [Serializable]
    public class FlowerManager : IUpdatable, IFlowerManager
    {
        #region Params
        
        private List<Flower> flowerList = new List<Flower>(1024);  
        

        [Header("Настройки для цветка")]
        [Tooltip("Максимальное количество меда в цветке")]  
        [SerializeField] private float flowerMaxHoney;


        [SerializeField] private float flowerHoneyRegeneration;
        
        [SerializeField] private float flowerStartHoney;
        
        [SerializeField] private int flowerMaxBeeCount;
        
        [Header("Настройки для менеджера цветков")] 
        [SerializeField] private int startCount;
        
        [SerializeField] private int maxCount;
        
        [SerializeField] private float newBornTime;
        
        private float currentTime = 0;
        
        private int flowerCounter = 0;

        private Field field;
        
        #endregion
  
        public List<Flower> FlowerList => flowerList;
        public List<Flower> GetFlowerList()
        {
            return FlowerList;
        }

        public event Action<Flower> FlowerBorn;

        #region Methods
        
        public void Initialize(Field _field, int _fieldStartCount, int _fieldMaxCount,
            float _fieldBornTime, float _flowerMaxHoney, float _flowerStartHoney,
            float _flowerHoneyRegeneration, int _flowerMaxBeeCount)
        {
            field = _field;
            startCount = _fieldStartCount;
            maxCount = _fieldMaxCount;
            newBornTime = _fieldBornTime;

            flowerMaxHoney = _flowerMaxHoney;
            flowerStartHoney = _flowerStartHoney;
            flowerHoneyRegeneration = _flowerHoneyRegeneration;
            flowerMaxBeeCount = _flowerMaxBeeCount;

            for (int i = 0; i < startCount; i++)
                CreateNewFlower();
            AllServices.Container.RegisterSingle<IFlowerManager>(this);
        }
        
        
        private void CreateNewFlower()
        {
            var flowerPosition = new Vector3(Random.Range(0, field.Size.x), 0, Random.Range(0, field.Size.y));
            var flower = new Flower(flowerList.Count, flowerMaxHoney, flowerStartHoney, flowerHoneyRegeneration, flowerMaxBeeCount, flowerPosition);
            flowerList.Add(flower);
            flower.OnDie += DeleteFlower;
            FlowerBorn?.Invoke(flower);
            flowerCounter++;
        }

        public void Update(float _deltaTime)
        {

            if (currentTime < newBornTime)
                currentTime += _deltaTime;
            else
            {
                if (startCount < maxCount)
                    CreateNewFlower();
                currentTime -= newBornTime;   
            }

            for (int i = flowerList.Count - 1; i >= 0; i--)
                flowerList[i].Update(_deltaTime);
            
        }
        private void DeleteFlower(Flower _flower)
        {
            var last = flowerList.Count - 1;
            var current = _flower.ArrayIndex;
            if (current != last)
            {
                flowerList[current] = flowerList[last];
                flowerList[current].ArrayIndex = current;
            }
            flowerList.RemoveAt(last);
            _flower.OnDie -= DeleteFlower;
        }

        public int GetFlowerCount() =>
            flowerCounter;

        #endregion
    }
}
