﻿using System;
using UnityEngine;

namespace Model.Runtime.Services
{
    [Serializable]
    public class StatisticManager : IStatistic
    {
        public void Initialize() =>
            AllServices.Container.RegisterSingle<IStatistic>(this);


        public void SaveTime(float timeCounter)
        {
            var time = PlayerPrefs.HasKey("TimeCounter") ? PlayerPrefs.GetFloat("TimeCounter") : 0;
            time += timeCounter;
            PlayerPrefs.SetFloat("TimeCounter", time);
            
        }

        public void SaveData(string dataKey, int data)
        {
            var savedData = PlayerPrefs.HasKey(dataKey) ? PlayerPrefs.GetInt(dataKey) : 0;
            savedData += data;
            PlayerPrefs.SetInt(dataKey, savedData);
        }
    }
}