﻿using System;
using UnityEngine;

namespace BeeGarden.Model
{
    /// <summary>
    /// Класс полюшки-поля
    /// </summary>
    [Serializable]
    public class Field
    {
        /// <summary>
        /// Площадь пасеки, доступная для действий
        /// </summary>
        [SerializeField] private Vector2 size;

        /// <summary>
        /// Ширина границы
        /// </summary>
        [SerializeField] private float border;

        public Vector2 Size => size;

        public float Border => border;
    }
}